import React, { Component } from "react";

// import { connect } from "react-redux";
import { Link, NavLink } from "react-router-dom";
// import classNames from 'classnames';
import {
  Navbar,
  Nav,
  Container,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import cookies from "js-cookie";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBackspace } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router";

const Header = (props) => {
  const history = useHistory();
  function handleRemoveCookies() {
    cookies.remove("userData");
    history.push({
      pathname: "/",
    });

    //   console.log(" ====:>> ");
  }

  return (
    <>
      <Navbar
        style={{ backgroundColor: "#f5f5f5", color: "#f06292" }}
        variant="dark"
        fixed="top"
      >
        <Navbar.Brand href="#home" style={{ color: "#f06292" }}>
          Sanchez Store
        </Navbar.Brand>
        <Nav className="ml-auto">
          <Nav.Link onClick={handleRemoveCookies}>
            <FontAwesomeIcon
              icon={faBackspace}
              size="lg"
              color="rgb(240 98 146)"
            />
          </Nav.Link>
          {/* <Nav.Link href="#">
              <img src={shoppingCart} width="40px" alt="Compras" title="Compras" />
            </Nav.Link> */}
          {/* <h3>hello</h3> */}
        </Nav>
      </Navbar>
    </>
  );
};

// const mapStateToProps = ({ login }) => ({ login });

// const mapDispatchToProps = {
//   logoutRequest,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Header);
export default Header;
