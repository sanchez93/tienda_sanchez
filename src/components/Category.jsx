import React from "react";

import "../assets/styles/Home.css";

export default function Category(props) {
  function getUrl(param) {
    return `https://io.tissini.app${param}`;
  }

  return (
    <>
      {props.categorysections &&
        props.categorysections.map((item) => (
          <>
            <img src={getUrl(item.image)} width="100%" />
            <ul className="ulproducts">
              {item.products &&
                item.products.map((item2) => (
                  <li>
                    <figure>
                      <img src={getUrl(item2.image.url)} alt="" />
                      <figcaption>
                        <p>
                          <strong>{item2.name}</strong>
                        </p>
                        <p>
                          <strong>{item2.categories.category}</strong>
                        </p>
                        <p>
                          <strong>{item2.price}</strong>
                        </p>
                      </figcaption>
                    </figure>
                  </li>
                ))}
            </ul>
          </>
        ))}
    </>
  );
}
