import React, { Component } from "react";

import { Link } from "react-router-dom";
import { Navbar, Nav, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faAddressBook,
  faDolly,
} from "@fortawesome/free-solid-svg-icons";

const Footer = () => {
  return (
    <>
      <Navbar
        style={{ backgroundColor: "#f5f5f5", color: "#f06292" }}
        variant="dark"
        fixed="bottom"
      >
        <Nav style={{ width: "100%", textAlign: "center" }}>
          <Row style={{ width: "100%" }}>
            <Link to="/categorias">
              <Col xs="4">
                <FontAwesomeIcon
                  icon={faHome}
                  size="2x"
                  color="rgb(240 98 146)"
                />
                <h5>Categoria</h5>
              </Col>
            </Link>
            <Link to="/catalogo">
              <Col xs="4">
                <FontAwesomeIcon
                  icon={faAddressBook}
                  size="2x"
                  color="rgb(240 98 146)"
                />
                <h5>Catalogo</h5>
              </Col>
            </Link>
            <Link to="/categorias">
              <Col xs="4">
                <FontAwesomeIcon
                  icon={faDolly}
                  size="2x"
                  color="rgb(240 98 146)"
                />
                <h5>Carrito</h5>
              </Col>
            </Link>
          </Row>
        </Nav>
      </Navbar>
    </>
  );
};
export default Footer;
