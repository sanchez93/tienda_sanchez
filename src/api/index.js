const axios = require("axios");

const mainRoute = "https://io.tissini.app/api/v1";

const listCategorysections = async () => {
  try {
    const data = await axios.get(`${mainRoute}/categories/sections`);
    return data.data;
  } catch (error) {
    return { error: true, message: "Error en la consulta" };
  }
};

const listActive = async () => {
  try {
    const { data } = await axios.get(`${mainRoute}/categories`);
    return data.body;
  } catch (error) {
    return { error: true, message: "Error en la consulta" };
  }
};

const detail = async (id) => {
  try {
    const { data } = await axios.get(`${mainRoute}/${id}`);
    return data.body;
  } catch (error) {
    return { error: true, message: "Error en la consulta" };
  }
};

const login = async (idUser) => {
  try {
    const data = await axios.post(`${mainRoute}/login/client`, {
      mobilephone: `${idUser}`,
    });
    return data.data;
  } catch (error) {
    return { error: true, message: "Error en la consulta" };
  }
};

const update = async (id) => {
  try {
    const { data } = await axios.put(`${mainRoute}/${id}`);
    return data.body;
  } catch (error) {
    return { error: true, message: "Error en la consulta" };
  }
};

const remove = async (id) => {
  try {
    const { data } = await axios.delete(`${mainRoute}/${id}`);
    return data.body;
  } catch (error) {
    console.log("error", error);
    return { error: true, message: "Error en la consulta" };
  }
};

module.exports = {
  listCategorysections,
  listActive,
  detail,
  login,
  update,
  remove,
};
