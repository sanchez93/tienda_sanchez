import React from "react";

import Header from "../components/Header";
import Footer from "../components/Footer";
import Category from "../components/Category";

import { listCategorysections } from "../api";

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      categorysections: [],
    };
  }

  async loadData() {
    const categorysections = await listCategorysections();

    console.log("categoriesList :>> ", categorysections);

    this.setState({ categorysections });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    return (
      <>
        <Header />
        <div className="container">
          <br />
          <br />
          <br />
          <Category {...this.state} />
        </div>
        <Footer />
      </>
    );
  }
}
// const mapStateToProps = ({ login, from }) => ({ login, from });

export default Home;
