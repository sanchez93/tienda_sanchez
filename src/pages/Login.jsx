import React from "react";

import { Form } from "react-bootstrap";

import logo from "../assets/static/logo.png";
import "../assets/styles/Login.css";
import { login } from "../api";
import cookies from "js-cookie";
import loading from "../assets/static/loading.gif";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      phone: "",
      error: false,
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleLogin(phone) {
    this.setState({ error: false, loading: true });
    const result = await login(phone);
    if (result.customer) {
      console.log(" result.customer :>> ", result.customer);
      cookies.set("userData", result.customer);
      setTimeout(() => this.props.history.push("/categorias"), 1000);
    } else {
      this.setState({ error: true, loading: false });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.handleLogin(this.state.phone);
  }
  handleChange(event) {
    const phone = event.target.value;
    phone.length === 10 && this.handleLogin(phone);
    this.setState({ phone });
  }

  // handleInput(event) {
  //   console.log("handleInput");
  //   const { form } = this.state;
  //   this.setState({
  //     form: {
  //       ...form,
  //       [event.target.name]: event.target.value,
  //     },
  //   });
  // }

  // componentDidMount() {
  //   cookies.remove("userData");
  //   console.log(" ====:>> ");
  // }

  // componentDidUpdate(prevProps) {
  //   if (
  //     prevProps.login.hasError !== this.props.login.hasError &&
  //     this.props.login.hasError
  //   ) {
  //     this.props.login.hasError = false;
  //     // message(this.props.login.error, 3);
  //   }
  // }

  render() {
    return (
      <section className="top-container-full-width">
        <div className="top-container">
          <a href="/" className="mb-3">
            <img src={logo} alt="Logo" />
          </a>
          <h3 className="mt-3">
            Ingresa el número de teléfono
            <br /> de tu asesora independiente.
          </h3>
          {/* <div className="login-container"> */}
          <Form onSubmit={this.handleSubmit}>
            <Form.Group>
              <Form.Control
                size="lg"
                type="text"
                placeholder="Telefono"
                onChange={this.handleChange}
              />
            </Form.Group>

            {this.state.loading && (
              <div className="text-center">
                <img src={loading} alt="loading" />
              </div>
            )}
          </Form>
          {/* </div> */}
        </div>
      </section>
    );
  }
}
// const mapStateToProps = ({ login, from }) => ({ login, from });

export default Login;
