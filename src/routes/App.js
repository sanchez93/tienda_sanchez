import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Login from "../pages/Login";
import Home from "../pages/Home";
import PrivateRoute from "./PrivateRoute";
import cookies from "js-cookie";

const App = () => {
  const userData = cookies.get("userData");
  console.log("userData", userData);
  return (
    <>
      <BrowserRouter>
        <Route exact path="/" component={Login}></Route>
        <PrivateRoute
          exact
          path="/categorias"
          component={Home}
          isLogged={!!userData}
        ></PrivateRoute>
      </BrowserRouter>
    </>
  );
};

export default App;
